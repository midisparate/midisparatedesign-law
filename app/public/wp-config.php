<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'PNPVGqMTMgW9HTnKmNTLp7pd3NG84LxBKAwLW9vLgR7iVusg6cBhsNfWS7DCwmSYNeu23BJwDvDsuf3YU3yYAQ==');
define('SECURE_AUTH_KEY',  'tMw5BxIngwVrq+QiYJ2UTXEtor4/1r9+qzNs1oPsaphZag06kBDh5Gbc+16OE45eHK2iS7Oa9GP9QNJzNsnBDg==');
define('LOGGED_IN_KEY',    'Wll7aIptbRmx4krHiBl4mJA+s9WnDTRkGCp5dW7U+1AlrYkDpyRK1MvmqmzFxrIy06Y4eZVTvZy8T+4zJaFy6w==');
define('NONCE_KEY',        'xXEI20XbsHNf/v+zJzIBfc+IIggX3/xCGX0p6BAiZ1JBVLw5QsrO6d7GjyLzBTFktCTtqo4/WvO8JSuwbmhz3A==');
define('AUTH_SALT',        'mO0gmLy2EYT4fL/NJJHB7xJBnFgzk7sINjab0PoTEXi1ieMzHCvBAKvvFU88LyYoqnQf3EZu/lTjPCSXIOcgAQ==');
define('SECURE_AUTH_SALT', 'pN48o9jPQw44+sjRHbHTJMkQ5I7w+80a+5mFsGEXIu7YxWVwsqooinaX/VbT4pVbrlBmgTZezfGpRhzqisw4gw==');
define('LOGGED_IN_SALT',   'Ix+V0/xOsCOZz3H6yxQgn9X5/E1rVqOfJw/IAE7n1FZ4YwahkUAMHYwWBQbRUCiK5Opdq+1RwvWvZcda1hlpkw==');
define('NONCE_SALT',       'WYwZOfL98hpQNV81CuwhoB1z0zncHytSqeXi/7maDqYmD6jSxwmMGG+JSnX56Igmj6Ixl1DgdL/mDfqF/ps9Bg==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
