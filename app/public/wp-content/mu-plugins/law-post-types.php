<?php 
function law_post_types() {
    // Firm Post Type
        register_post_type('firm', array(
            'capability_type' => 'firm',
            'map_meta_cap' => true,
            'show_in_rest' => true,
            'supports' => array('title', 'editor', 'excerpt'),
            'rewrite' => array('slug' => 'firm'),
            'has_archive' => true,
            'public' => true,
            'labels' => array(
                'name' => 'Firms',
                'add_new_item' => 'Add New Firm',
                'edit_item' => 'Edit Firm',
                'all_items' => 'All Firms',
                'singular_name' => 'Firm'
            ),
            'menu_icon' => 'dashicons-building'
        ));

    // Event Post Type
    register_post_type('event', array(
        'capability_type' => 'event',
        'map_meta_cap' => true,
        'show_in_rest' => true,
        'supports' => array('title', 'editor', 'excerpt'),
        'rewrite' => array('slug' => 'events'),
        'has_archive' => true,
        'public' => true,
        'labels' => array(
            'name' => 'Events',
            'add_new_item' => 'Add New Event',
            'edit_item' => 'Edit Event',
            'all_items' => 'All Events',
            'singular_name' => 'Event'
        ),
        'menu_icon' => 'dashicons-calendar'
    ));

    // Practice Areas Post Type
register_post_type('practice', array(
    'capability_type' => 'practice',
    'map_meta_cap' => true,
    'show_in_rest' => true,
    'supports' => array('title', 'excerpt', 'thumbnail'),
    'rewrite' => array('slug' => 'practice'),
    'has_archive' => true,
    'public' => true,
    'labels' => array(
        'name' => 'Practices',
        'add_new_item' => 'Add New Practice',
        'edit_item' => 'Edit Practice',
        'all_items' => 'All Practices',
        'singular_name' => 'Practice'
    ),
    'menu_icon' => 'dashicons-list-view'
));

   // Specialty Post Type
   register_post_type('specialty', array(
    'capability_type' => 'specialty',
    'map_meta_cap' => true,
    'show_in_rest' => true,
    'supports' => array('title', 'excerpt', 'thumbnail'),
    'rewrite' => array('slug' => 'specialty'),
    'has_archive' => true,
    'public' => true,
    'labels' => array(
        'name' => 'Specialties',
        'add_new_item' => 'Add New Specialty',
        'edit_item' => 'Edit Specialty',
        'all_items' => 'All Specialties',
        'singular_name' => 'Specialty'
    ),
    'menu_icon' => 'dashicons-star-filled'
));

// Staff Post Type
register_post_type('staff', array(
    'capability_type' => 'staff',
    'map_meta_cap' => true,
    'show_in_rest' => true,
    'supports' => array('title', 'editor', 'excerpt', 'thumbnail'),
    'rewrite' => array('slug' => 'staff'),
    'has_archive' => true,
    'public' => true,
    'labels' => array(
        'name' => 'Staff',
        'add_new_item' => 'Add New Staff',
        'edit_item' => 'Edit Staff',
        'all_items' => 'All Staff',
        'singular_name' => 'Staff'
    ),
    'menu_icon' => 'dashicons-groups'
));

// Note Post Type
register_post_type('note', array(
    'capability_type' => 'note',
    'map_meta_cap' => true,
    'show_in_rest' => true,
    'supports' => array('title', 'editor'),
    'public' => false,
    'show_ui' => true,
    'labels' => array(
      'name' => 'Notes',
      'add_new_item' => 'Add New Note',
      'edit_item' => 'Edit Note',
      'all_items' => 'All Notes',
      'singular_name' => 'Note'
    ),
    'menu_icon' => 'dashicons-welcome-write-blog'
  ));

  // Like Post Type
register_post_type('like', array(
    'supports' => array('title'),
    'public' => false,
    'show_ui' => true,
    'labels' => array(
      'name' => 'Likes',
      'add_new_item' => 'Add New Like',
      'edit_item' => 'Edit Like',
      'all_items' => 'All Likes',
      'singular_name' => 'Like'
    ),
    'menu_icon' => 'dashicons-heart'
  ));

}

add_action('init', 'law_post_types');
?>