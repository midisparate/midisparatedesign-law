import $ from 'jquery';


class Search {
    // 1. describe and create/initiate our object
    constructor() {
        this.addSearchHTML();
        this.resultsDiv = $("#search-overlay__results");
        this.openButton = $(".js-search-trigger"); 
        this.closeButton = $(".search-overlay__close");
        this.searchOverlay = $(".search-overlay");
        this.searchField = $("#search-term");
        this.events();
        this.isOverlayOpen = false;
        this.isSpinnerVisible = false;
        this.previousValue;
        this.typingTimer;

    }

    // 2. events
    events() {
        this.openButton.on("click", this.openOverlay.bind(this)); 
        this.closeButton.on("click", this.closeOverlay.bind(this));
        $(document).on("keydown", this.keyPressDispatcher.bind(this));
        this.searchField.on("keyup", this.typingLogic.bind(this));
    }

    // 3. methods (funtion, action...)
    typingLogic(){
        if (this.searchField.val() != this.previousValue) {
            clearTimeout(this.typingTimer);

        if (this.searchField.val()) {
            if (!this.isSpinnerVisible){
                this.resultsDiv.html('<div class="spinner-loader"></div>');
                this.isSpinnerVisible = true;
            }
            this.typingTimer = setTimeout(this.getResults.bind(this), 750);
        } else {
            this.resultsDiv.html('');
            this.isSpinnerVisible = false;
        }

        }

        this.previousValue = this.searchField.val();
    }

    getResults(){
        $.getJSON(lawData.root_url + '/wp-json/midisparate/v1/search?term=' + this.searchField.val(), (results) => {
            this.resultsDiv.html(`
                <div class="row">
                <div class="one-third">
                    <h2 class="search-overlay__section-title">General Information</h2>
                    ${results.generalInfo.length ? '<ul class="link-list min-list">' : '<p>No general information matches that search.</p>' }
                    ${results.generalInfo.map(item => `<li><a href="${item.permalink}">${item.title}</a> ${item.postType == 'post' ? `by ${item.authorName}` : ''}</li>`).join('')}
                    ${results.generalInfo.length ? '</ul>' : ''}
                </div>
                <div class="one-third">
                    <h2 class="search-overlay__section-title">Practices</h2>
                    ${results.specialtys.length ? '<ul class="link-list min-list">' : '' }
                    ${results.specialtys.map(item => `<li><a href="${item.permalink}">${item.title}</a></li>`).join('')}
                    ${results.specialtys.length ? '</ul>' : ''}
                    ${results.practices.length ? '<ul class="link-list min-list">' : `<p>No practices match that search. <a href="${lawData.root_url}/practice">View all practices</a></p>` }
                    ${results.practices.map(item => `<li><a href="${item.permalink}">${item.title}</a></li>`).join('')}
                    ${results.practices.length ? '</ul>' : ''}

                    <h2 class="search-overlay__section-title">Staff</h2>
                    ${results.staffs.length ? '<ul class="link-list min-list">' : `<p>No staff match that search. <a href="${lawData.root_url}/staff">View all staff members</a></p>` }
                    ${results.staffs.map(item => `
                        <li class="professor-card__list-item">
                        <a class="professor-card" href="${item.permalink}">
                        <img class="professor-card__image" src="${item.image}">
                        <span class="professor-card__name">${item.title}</span>
                        </a>
                    </li>
                    `).join('')}
                    ${results.staffs.length ? '</ul>' : ''}
                    
                </div>
                <div class="one-third">
                    <h2 class="search-overlay__section-title">Firm</h2>
                    ${results.firms.length ? '<ul class="link-list min-list">' : `<p>No firm match that search. <a href="${lawData.root_url}/firm">View all firms</a></p>` }
                    ${results.firms.map(item => `<li><a href="${item.permalink}">${item.title}</a></li>`).join('')}
                    ${results.firms.length ? '</ul>' : ''}
                    <h2 class="search-overlay__section-title">Events</h2>
                    ${results.events.length ? '' : `<p>No events match that search. <a href="${lawData.root_url}/events">View all events</a></p>` }
                    ${results.events.map(item => `
                    <div class="event-summary">
                        <a class="event-summary__date t-center" href="${item.permalink}">
                            <span class="event-summary__month">${item.month}</span>
                            <span class="event-summary__day">${item.day}</span>
                        </a>
                    <div class="event-summary__content">
                        <h5 class="event-summary__title headline headline--tiny"><a href="${item.permalink}">${item.title}</a></h5>
                        <p>${item.description} <a href="${item.permalink}" class="nu gray">Learn more</a></p>
                    </div>
                </div>
                    `).join('')}
                </div>
                </div>
            `);
            this.isSpinnerVisible = false;
        });
    }

    keyPressDispatcher(e){

        if (e.keyCode == 83 && !this.isOverlayOpen && !$("input, textarea").is(':focus')) {
            this.openOverlay();
        }

        if (e.keyCode == 27 && !this.isOverlayOpen) {
            this.closeOverlay();
        }
    }
    openOverlay() {
        this.searchOverlay.addClass("search-overlay--active");
        $("body").addClass("body-no-scroll");
        this.searchField.val('');
        setTimeout(() => this.searchField.focus(), 301);
        console.log("our open method just ran!");
        this.isOverlayOpen = true;
        return false; 
    }
    closeOverlay() {
        this.searchOverlay.removeClass("search-overlay--active");
        $("body").removeClass("body-no-scroll");
        console.log("our close method just ran!");
        this.isOverlayOpen = false; 
    }

    addSearchHTML() {
        $("body").append(`
        <div class="search-overlay">
        <div class="search-overlay__top">
          <div class="container">
            <i class="fa fa-search search-overlay__icon" aria-hidden="true"></i>
            <input type="text" class="search-term" placeholder="What are you looking for?" id="search-term" autocomplete="off">
            <i class="fa fa-window-close search-overlay__close" aria-hidden="true"></i>
          </div>
        </div>
  
        <div class="container">
         <div id="search-overlay__results"></div>           
        </div>
  
      </div>
        `);
    }
}

export default Search