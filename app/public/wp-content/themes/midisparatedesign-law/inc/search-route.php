<?php

add_action('rest_api_init', 'midisparateRegisterSearch');

function midisparateRegisterSearch() {
  register_rest_route('midisparate/v1', 'search', array(
    'methods' => WP_REST_SERVER::READABLE,
    'callback' => 'midisparateSearchResults'
  ));
}

function midisparateSearchResults($data) {
  $mainQuery = new WP_Query(array(
    'post_type' => array('post', 'page', 'staff', 'specialty', 'practice', 'firm', 'event'),
    's' => sanitize_text_field($data['term'])
  ));

  $results = array(
    'generalInfo' => array(),
    'staffs' => array(),
    'specialtys' => array(),
    'practices' => array(),
    'events' => array(),
    'firms' => array()
  );

  while($mainQuery->have_posts()) {
    $mainQuery->the_post();

    if (get_post_type() == 'post' OR get_post_type() == 'page') {
      array_push($results['generalInfo'], array(
        'title' => get_the_title(),
        'permalink' => get_the_permalink(),
        'postType' => get_post_type(),
        'authorName' => get_the_author()
      ));
    }

    if (get_post_type() == 'staff') {
      array_push($results['staffs'], array(
        'title' => get_the_title(),
        'permalink' => get_the_permalink(),
        'image' => get_the_post_thumbnail_url(0, 'facultyLandscape')
      ));
    }

    if (get_post_type() == 'specialty') {
        array_push($results['specialtys'], array(
          'title' => get_the_title(),
          'permalink' => get_the_permalink()
        ));
      }
    
      if (get_post_type() == 'practice') {
        $relatedLocation = get_field('related_firm');

        if($relatedLocation) {
            foreach($relatedLocation as $location) {
                array_push($results['firms'], array(
                    'title' => get_the_title($location),
                    'permalink' => get_the_permalink($location)
                ));
            }
        }}

    if (get_post_type() == 'practice') {
      array_push($results['practices'], array(
        'title' => get_the_title(),
        'permalink' => get_the_permalink(),
        'id' => get_the_id()
      ));
    }

    if (get_post_type() == 'firm') {
      array_push($results['firms'], array(
        'title' => get_the_title(),
        'permalink' => get_the_permalink()
      ));
    }

    if (get_post_type() == 'event') {
        $eventDate = new DateTime(get_field('event_date'));
        $description = null;
        if (has_excerpt()) {
                $description = get_the_excerpt();
            } else { 
                $description = wp_trim_words(get_the_content(), 18); 
            }
    
          array_push($results['events'], array(
            'title' => get_the_title(),
            'permalink' => get_the_permalink(),
            'month' => $eventDate->format('M'),
            'day' => $eventDate->format('d'),
            'description' => $description
          ));
        }
    
  }

  if ($results['practices']) {
    $midiMetaQuery = array('relation' => 'OR');

    foreach($results['practices'] as $item) {
      array_push($midiMetaQuery, array(
          'key' => 'related_practices',
          'compare' => 'LIKE',
          'value' => '"' . $item['id'] . '"'
        ));
    }

    $midiRelationshipQuery = new WP_Query(array(
      'post_type' => array('staff', 'event'),
      'meta_query' => $midiMetaQuery
    ));

    while($midiRelationshipQuery->have_posts()) {
      $midiRelationshipQuery->the_post();

      if (get_post_type() == 'event') {
        $eventDate = new DateTime(get_field('event_date'));
        $description = null;
        if (has_excerpt()) {
          $description = get_the_excerpt();
        } else {
          $description = wp_trim_words(get_the_content(), 18);
        }

        array_push($results['events'], array(
          'title' => get_the_title(),
          'permalink' => get_the_permalink(),
          'month' => $eventDate->format('M'),
          'day' => $eventDate->format('d'),
          'description' => $description
        ));
      }

      if (get_post_type() == 'staff') {
        array_push($results['staffs'], array(
          'title' => get_the_title(),
          'permalink' => get_the_permalink(),
          'image' => get_the_post_thumbnail_url(0, 'staffLandscape')
        ));
      }

    }

    $results['staffs'] = array_values(array_unique($results['staffs'], SORT_REGULAR));
    $results['events'] = array_values(array_unique($results['events'], SORT_REGULAR));
  }


  return $results;

}