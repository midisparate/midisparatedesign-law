<?php 
require get_theme_file_path('/inc/like-route.php');
require get_theme_file_path('/inc/search-route.php');

function midisparate_custom_rest() {
    register_rest_field('post', 'authorName', array(
        'get_callback' => function() {return get_the_author();}  
    ));

    register_rest_field('note', 'userNoteCount', array(
        'get_callback' => function() {return count_user_posts(get_current_user_id(), 'note');}
    ));
}

function pageBanner($args = NULL) {
    
    if (!$args['title']) {
        $args['title'] = get_the_title();
    }

    if (!$args['subtitle']) {
        $args['subtitle'] = get_field('page_banner_subtitle');
    }

    if (!$args['photo']) {
        if (get_field('page_banner_background_image') AND !is_archive() AND !is_home()) {
        $args['photo'] = get_field('page_banner_background_image') ['sizes'] ['pageBanner'];
    } else {
        $args['photo'] = get_theme_file_uri('/images/subhero.jpg');
    }
    }

    ?>
    <div class="page-banner">
    <div class="page-banner__bg-image" style="background-image: url(<?php echo $args['photo']; ?>)"></div>
      <div class="page-banner__content container container--narrow">
        <h1 class="page-banner__title"><?php echo $args['title'] ?></h1>
        <div class="page-banner__intro">
          <p><?php echo $args['subtitle'] ?></p>
        </div>
    </div>
  </div>
<?php }

function law_files(){
    wp_enqueue_script('googleMap-js', '//maps.googleapis.com/maps/api/js?key=AIzaSyAloBbizHwIo0wm7L3s8auf5voUB1_19ko', NULL, '1.0', true);    
    wp_enqueue_script('main-law-js', get_theme_file_uri('/build/index.js'), array('jquery'), '1.0', true);
    wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
    wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
    wp_enqueue_style('law_main_styles', get_theme_file_uri('/build/style-index.css'));
    wp_enqueue_style('law_extra_styles', get_theme_file_uri('/build/index.css'));

    wp_localize_script('main-law-js', 'lawData', array(
        'root_url' => get_site_url(),
        'nonce' => wp_create_nonce('wp_rest')
    ));

}
add_action('wp_enqueue_scripts', 'law_files');

function law_features(){
    register_nav_menu('headerMiniMenu', 'Header Mini Menu');
    register_nav_menu('footerLeftMenu', 'Footer Left Menu');
    register_nav_menu('footerRightMenu', 'Footer Right Menu');
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
    add_image_size('staffLandscape', 400, 260, true);
    add_image_size('staffPortrait', 480, 650, true);
    add_image_size('pageBanner', 1500, 350, true);
}

add_action('after_setup_theme', 'law_features');

function law_adjust_queries($query) {
    if (!is_admin() AND is_post_type_archive('firm') AND is_main_query()) {
        $query->set('post_per_page', -1);
    }

    if (!is_admin() AND is_post_type_archive('practice') AND is_main_query()) {
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        $query->set('post_per_page', -1);
    }

    if (!is_admin() AND is_post_type_archive('specialty') AND is_main_query()) {
        $query->set('orderby', 'title');
        $query->set('order', 'ASC');
        $query->set('post_per_page', -1);
    }

    if (!is_admin() AND is_post_type_archive('event') AND $query->is_main_query()) {
        $today = date('Ymd');
        $query->set('meta_key', 'event_date');
        $query->set('orderby', 'meta_value_num');
        $query->set('order', 'ASC');
        $query->set('meta_query', array(
                array(
                'key' => 'event_date',
                'compare' => '>=',
                'value' => $today,
                'type' => 'numeric'
            )
          ));
    }
}

add_action('pre_get_posts', 'law_adjust_queries');

function lawMapKey($api) {
    $api['key'] = 'AIzaSyAloBbizHwIo0wm7L3s8auf5voUB1_19ko';
    return $api;
}

add_filter('acf/fields/google_map/api', 'lawMapKey');

// Redirect subscriber accounts out of admin and onto hamepage

add_action('admin_init','redirectSubsTOFrontend');

function redirectSubsTOFrontend() {
    $ourCurrentUser = wp_get_current_user();

    if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles [0] == 'subcriber') {
        wp_redirect(site_user('/'));
        exit;
    }
}

add_action('wp_loaded','noSubsAdminBar');

function noSubsAdminBar() {
    $ourCurrentUser = wp_get_current_user();

    if (count($ourCurrentUser->roles) == 1 AND $ourCurrentUser->roles [0] == 'subcriber') {
        show_admin_bar(false);
    }
}

// Customize Login Screen
add_filter('login_headerurl', 'ourHeaderUrl');

function ourHeaderUrl() {
  return esc_url(site_url('/'));
}

add_action('login_enqueue_scripts', 'ourLoginCSS');

function ourLoginCSS() {
  wp_enqueue_style('custom-google-fonts', '//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i');
  wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
  wp_enqueue_style('university_main_styles', get_theme_file_uri('/build/style-index.css'));
  wp_enqueue_style('university_extra_styles', get_theme_file_uri('/build/index.css'));
}

add_filter('login_headertitle', 'ourLoginTitle');

function ourLoginTitle() {
  return get_bloginfo('name');
}

// Force note posts to be private
add_filter('wp_insert_post_data', 'makeNotePrivate', 10, 2);

function makeNotePrivate($data, $postarr) {
  if ($data['post_type'] == 'note') {
    if(count_user_posts(get_current_user_id(), 'note') > 9 AND !$postarr['ID']) {
      die("You have reached your note limit.");
    }

    $data['post_content'] = sanitize_textarea_field($data['post_content']);
    $data['post_title'] = sanitize_text_field($data['post_title']);
  }

  if($data['post_type'] == 'note' AND $data['post_status'] != 'trash') {
    $data['post_status'] = "private";
  }
  
  return $data;
}