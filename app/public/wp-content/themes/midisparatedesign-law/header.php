<!DOCTYPE html>
<html>
    <head>
      <meta charset="<?php bloginfo('charset'); ?>">
      <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header class="site-header">
      <div class="container">
        <!------ Logo Area - Start  ------>
        <h1 class="school-logo-text float-left">
          <a href="<?php echo site_url() ?>"><strong>Mi Disparate</strong> Law</a>
        </h1>
        <!------ Logo Area - End  ------>
        <a href="<?php echo esc_url(site_url('/search'));?>" class="js-search-trigger site-header__search-trigger"><i class="fa fa-search" aria-hidden="true"></i></a>
        <i class="site-header__menu-trigger fa fa-bars" aria-hidden="true"></i>
        <div class="site-header__menu group">
        <!------ Mini Menu Top Area - Start  ------>

        <!------ Mini Menu Top Area - End  ------>
        <!------ Main Menu Area - Start  ------>
          <nav class="main-navigation">
          <ul>
              <li <?php if (is_page('home') or wp_get_post_parent_id(0) == 35 ) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('<?php echo site_url() ?>') ?>">Home</a></li>
              <li <?php if (is_page('about-us') or wp_get_post_parent_id(0) == 11 ) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/about-us') ?>">About Us</a></li>
              <li <?php if (get_post_type() == 'practice') echo 'class="current-menu-item"' ?>><a href="<?php echo get_post_type_archive_link('practice') ?>">Our Practice</a></li>
              <li <?php if (get_post_type() == 'Firm') echo 'class="current-menu-item"' ?>><a href="<?php echo get_post_type_archive_link('firm') ?>">Our Firm</a></li>
              <li <?php if (is_page('attorneys') or wp_get_post_parent_id(0) == 9 ) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/attorneys') ?>">Attorneys</a></li>
              <li <?php if (get_post_type() == 'event' OR is_page('past-events')) echo 'class="current-menu-item"' ?>><a href="<?php echo get_post_type_archive_link('event') ?>">Events</a></li>              
              <li <?php if (get_post_type() == 'post') echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/news') ?>">News</a></li>
              <li <?php if (is_page('contact-us') or wp_get_post_parent_id(0) == 9 ) echo 'class="current-menu-item"' ?>><a href="<?php echo site_url('/contact-us') ?>">Contact Us</a></li>
            </ul>
          </nav>
          <!------ Main Menu Area - End  ------>
          <!------ Right Btn Menu Area - Start  ------>
          <div class="site-header__util">
          <?php if(is_user_logged_in()) { ?>
            <a href="<?php echo esc_url(site_url('/my-notes')); ?>" class="btn btn--small btn--orange float-left push-right">My Notes</a>
            <a href="<?php echo wp_logout_url();  ?>" class="btn btn--small  btn--dark-orange float-left btn--with-photo">
            <span class="site-header__avatar"><?php echo get_avatar(get_current_user_id(), 60); ?></span>
            <span class="btn__text">Log Out</span>
            </a>
            <?php } else { ?>
              <a href="<?php echo wp_login_url(); ?>" class="btn btn--small btn--orange float-left push-right">Login</a>
              <a href="<?php echo wp_registration_url(); ?>" class="btn btn--small  btn--dark-orange float-left">Sign Up</a>
             <?php } ?>
          
          <a href="<?php echo esc_url(site_url('/search')); ?>" class="search-trigger js-search-trigger"><i class="fa fa-search" aria-hidden="true"></i></a>
        </div>
          <!------ Right Btn Menu Area - End  ------>
      </div>
    </header>