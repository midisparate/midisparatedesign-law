<?php 
get_header();
pageBanner(array(
  'title' => 'Practices',
  'subtitle' => 'There is a practice we provide for you. Have look around.'
));
 ?>
 
<div class="container container--narrow page-section">

<ul class="link-list min-list">

<?php 
while(have_posts()) {
  the_post(); ?>
   <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
              <p class="event-summary__title headline headline--tiny"><?php if (has_excerpt()) {
                echo get_the_excerpt();
              } else { 
                echo wp_trim_words(get_the_content(), 18); 
              }?> 
              <a href="<?php the_permalink(); ?>" class="nu gray">Read More</a></p>
     </li>
   
<?php }
echo paginate_links();
?>
</ul>

</div>
<?php get_footer(); ?>