<?php  get_header();
pageBanner();
 ?>

<div class="container container--narrow page-section">
<div class="metabox metabox--position-up metabox--with-home-link">
        <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('staff'); ?>"><i class="fa fa-home" aria-hidden="true"></i> Staff Home</a> <span class="metabox__main"><?php the_title(); ?></span></p>
</div>

<div class="generic-content">
  <div class="row group">
    <div class="one-third"><?php the_post_thumbnail('staffPortrait'); ?></div>
    <div class="two-third">
    <?php 
      
      $likeCount = new WP_Query(array(
        'post_type' => 'like',
        'meta_query' => array(
          array(
            'key' => 'liked_professor_id',
            'compare' => '=',
            'value' => get_the_ID()
          )
        )
          ));

      $existStatus = 'no';    

      $existQuery = new WP_Query(array(
        'author' => get_current_user_id(),
        'post_type' => 'like',
        'meta_query' => array(
          array(
            'key' => 'liked_professor_id',
            'compare' => '=',
            'value' => get_the_ID()
          )
        )
          ));

          if ($existQuery->found_posts) {
            $existStatus = 'yes';
          }
      ?>

      <spam class="like-box" data-exists="<?php echo $existStatus; ?>">
        <i class="fa fa-heart-o" aria-hidden="true"></i>
        <i class="fa fa-heart" aria-hidden="true"></i>
        <spam class="like-count"><?php echo $likeCount->found_posts; ?></spam>
      </spam>  
    <?php the_content(); ?></div>
</div>
</div>

<?php

$relatedPractices = get_field('related_practices');

if ($relatedPractices) {
  echo '<hr class="section-break">';
  echo '<h2 class="headline headline--medium">Related Practice(s)</h2>';
  echo '<ul class="link-list min-list">';
  foreach($relatedPractices as $practices) { ?>
    <li><a href="<?php echo get_the_permalink($practices); ?>"><?php echo get_the_title($practices); ?></a></li>
  <?php }
  echo '</ul>';
}
?>

</div>   
<?php get_footer(); ?>