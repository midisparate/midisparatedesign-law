<?php 

function law_files(){
    wp_enqueue_script('law-themepunch.tools-js', get_theme_file_uri('/revolution/js/jquery.themepunch.tools.min.js?rev=5.0'), NULL, '1.0', true);
    wp_enqueue_script('law-themepunch.revolution-js', get_theme_file_uri('/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0'), NULL, '1.0', true);
    wp_enqueue_script('law-slideanims-js', get_theme_file_uri('/revolution/js/extensions/revolution.extension.slideanims.min.js'), NULL, '1.0', true);
    wp_enqueue_script('law-layeranimation-js', get_theme_file_uri('/revolution/js/extensions/revolution.extension.layeranimation.min.js'), NULL, '1.0', true);
    wp_enqueue_script('law-extension.navigation-js', get_theme_file_uri('/revolution/js/extensions/revolution.extension.navigation.min.js'), NULL, '1.0', true);
    wp_enqueue_script('law-extension.parallax-js', get_theme_file_uri('/revolution/js/extensions/revolution.extension.parallax.min.js'), NULL, '1.0', true);
    wp_enqueue_script('law-jquery-js', get_theme_file_uri('/src/jquery-1.11.3.min.js'), NULL, '1.0', true);
    wp_enqueue_script('law-bootstrap-js', get_theme_file_uri('/src/bootstrap.min.js'), NULL, '1.0', true);
    wp_enqueue_script('law-owl.carousel-js', get_theme_file_uri('/src/owl.carousel.js'), NULL, '1.0', true);
    wp_enqueue_script('law-menuzord-js', get_theme_file_uri('/src/menuzord.js'), NULL, '1.0', true);
    wp_enqueue_script('law-countdown-js', get_theme_file_uri('/src/countdown.js'), NULL, '1.0', true);
    wp_enqueue_script('law-jquery.counterup-js', get_theme_file_uri('/src/jquery.counterup.js'), NULL, '1.0', true);
    wp_enqueue_script('law-waypoints-js', get_theme_file_uri('/src/waypoints.min.js'), NULL, '1.0', true);
    wp_enqueue_script('law-googleapis-js', '//maps.googleapis.com/maps/api/js', NULL, '1.0', true);
    wp_enqueue_script('law-bootFolio-js', get_theme_file_uri('/src/jquery.bootFolio.js'), NULL, '1.0', true);
    wp_enqueue_script('law-magnific-js', get_theme_file_uri('/src/jquery.magnific-popup.js'), NULL, '1.0', true);
    wp_enqueue_script('law-jquery-ui-js', get_theme_file_uri('/src/jquery-ui.js'), NULL, '1.0', true);
    wp_enqueue_script('law-rev-slider-js', get_theme_file_uri('/src/rev-slider.js'), NULL, '1.0', true);
    wp_enqueue_script('law-main-js', get_theme_file_uri('/src/main.js'), NULL, '1.0', true);
    wp_enqueue_style('google-fonts-lora', '//fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic');
    wp_enqueue_style('google-fonts-raleway', '//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900');
    wp_enqueue_style('google-fonts-OpenSans', '//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic');
    wp_enqueue_style('law_main_styles', get_theme_file_uri('/css/main.css'));
    wp_enqueue_style('law_pagebanner_styles', get_theme_file_uri('/css/page-banner.css'));
    wp_enqueue_style('law_bootFolio_styles', get_theme_file_uri('/css/bootFolio.css'));
    wp_enqueue_style('law_bootstrap-theme_styles', get_theme_file_uri('/css/bootstrap-theme.css'));
    wp_enqueue_style('law_bootstrap-theme.min_styles', get_theme_file_uri('/css/bootstrap-theme.min.css'));
    wp_enqueue_style('law_bootstrap_styles', get_theme_file_uri('/css/bootstrap.css'));
    wp_enqueue_style('law_bootstrap.min_styles', get_theme_file_uri('/css/bootstrap.min.css'));
    wp_enqueue_style('law_font-awesome.min_styles', get_theme_file_uri('/css/font-awesome.min.css'));
    wp_enqueue_style('law_icon-stroke_styles', get_theme_file_uri('/css/icon-stroke.css'));
    wp_enqueue_style('law_jquery-ui_styles', get_theme_file_uri('/css/jquery-ui.css'));
    wp_enqueue_style('law_jquery.countdownTimer_styles', get_theme_file_uri('/css/jquery.countdownTimer.css'));
    wp_enqueue_style('law_magnific-popup_styles', get_theme_file_uri('/css/magnific-popup.css'));
    wp_enqueue_style('law_menuzord_styles', get_theme_file_uri('/css/menuzord.css'));
    wp_enqueue_style('law_owl.carousel_styles', get_theme_file_uri('/css/owl.carousel.css'));
    wp_enqueue_style('law_owl.theme_styles', get_theme_file_uri('/css/owl.theme.css'));
}
add_action('wp_enqueue_scripts', 'law_files');

function law_features(){
    add_theme_support('title-tag');
}

add_action('after_setup_theme', 'law_features');

?>