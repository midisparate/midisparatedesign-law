<!DOCTYPE html>
<html>
    <head>
        <?php wp_head(); ?>
</head>
<body>
 <!-- =========================
     START WELCOME SECTION
    ============================== -->
    <header class="v3-header">
        <div class="v3-header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="v-3-header-contact">
                            <ul>
                               <li><span>Phone :</span> + 2 0106 5370701</li>
                               <li><span>Email :</span> 7oroof@7oroof.com</li>
                               <li><a href="<?php echo site_url('/free-consultation') ?>">Free Consultation</a></li>
                            </ul>
                        </div>                        
                    </div>
                    <div class="col-md-6">
                        <div class="v-3-header-contact v-3-header-contact-social">
                            <ul>
                               <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a></li>
                               <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Rss"><i class="fa fa-rss"></i></a></li>
                               <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a></li>
                               <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                               <li><a href="#" data-toggle="tooltip" data-placement="bottom" title="Vimeo"><i class="fa fa-vimeo-square"></i></a></li>
                            </ul>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <nav class="v3-header-bg">
            <div class="container custom-header">
                <div class="row">
                    <div class="v-3-menuzord clearfix">
                        <div id="menuzord" class="menuzord">
                           <a href="<?php echo site_url() ?>" class="menuzord-brand"><img src="<?php echo get_theme_file_uri('/images/horse.png') ?>" width="35" height="51" alt="">Trust <span>We provide legal solutions <br>for you !</span></a>         
                           <ul class="menuzord-menu header-v3-menuzord-menu menuzord-menu-bg">
                                <li><a href="<?php echo site_url('') ?>">HOME</a></li>
                                <li><a href="<?php echo site_url('/about-us') ?>">ABOUT</a></li>
                                <li><a href="<?php echo site_url('/practice-areas') ?>">PRACTICE AREAS</a></li>
                                <li><a href="<?php echo site_url('/attorneys') ?>">ATTORNEYS</a></li>
                                <li><a href="<?php echo site_url('/blog') ?>">BLOG</a></li>
                                <li><a href="<?php echo site_url('/features') ?>">FEATURES</a></li>
                                <li><a href="<?php echo site_url('/shop') ?>">SHOP</a></li>                    
                                <li><a href="<?php echo site_url('/contact-us') ?>">CONTACT US</a></li>
                           </ul>
                        </div>
                    </div>
                </div>
            </div>            
        </nav>
    </header>
