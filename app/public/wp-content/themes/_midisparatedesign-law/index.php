<?php get_header(); ?>

<!-- =========================
     START SLIDER SECTION
    ============================== -->

    <!-- START REVOLUTION SLIDER 5.0 -->
    <div class="page-banner">
      <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/library-hero.jpg') ?>)"></div>
      <div class="page-banner__content container t-center c-white">
        <h1 class="headline headline--large">Welcome!</h1>
        <h2 class="headline headline--medium">We think you&rsquo;ll like it here.</h2>
        <h3 class="headline headline--small">Why don&rsquo;t you check out the <strong>major</strong> you&rsquo;re interested in?</h3>
        <a href="#" class="btn btn--large btn--blue">Find Your Major</a>
      </div>
    </div>
    <!-- END OF SLIDER WRAPPER -->

    <!-- START HOME 3 -->
    <div class="home-3-contact">
        <div class="container">
            <div class="row no-margin">
            <div class="home-3-contact-details clearfix">
                <div class="col-md-8">
                    <div class="h-3-contact-text">
                        <p>Trust is a business theme perfectly suited legal advisers and offices, lawyers, attorneys, counsels, advocates and other legal and law related services.</p>
                    </div>
                </div>
                <div class="col-md-4 no-padding">
                    <div class="h-3-contact-button">
                        <button type="button" class="btn btn-default">Contact Us</button>
                        <button type="button" class="btn btn-default">All Practice Areas</button>
                    </div>
                </div>
            </div> 
            </div> 
        </div>
    </div> <!-- end end content -->


    <!-- =========================
     START PRACTICE SECTION
    ============================== -->

    <section class="practice-area home-2-practice-area home-3-practice-area">
        <!-- start practice content area -->
        <div class="container">
            <div class="row">
                <div class="h-2-practice-content clearfix">
                    <div class="col-sm-6 col-md-3">
                        <div class="h-2-p-c-details">
                            <div class="h-2-p-c-default h-3-p-c-default">
                                <img src="images/home-3-blog-post-1.png" class="img-responsive" alt="">
                                <h2><a href="#">Family Law</a></h2>
                                <p>Family law consists of a body of statutes and case precedents that govern the legal responsibilities between individuals who share a domestic connection.</p>
                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="h-2-p-c-details">
                            <div class="h-2-p-c-default h-3-p-c-default">
                                <img src="images/home-3-blog-post-2.png" class="img-responsive" alt="">
                                <h2><a href="#">Fire Accident</a></h2>
                                <p>Fire accidents can result in catastrophic personal injury and devastating damage. Every year, billions of dollars in property damage occurs as a result of fire.</p>
                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="h-2-p-c-details h-3-p-c-default">
                            <div class="h-2-p-c-default">
                                <img src="images/home-3-blog-post-3.png" class="img-responsive" alt="">
                                <h2><a href="#">Shoplifting</a></h2>
                                <p>Shoplifting law deals with theft crimes that occur in retail establishments. Perpetrators are shoppers who enter the establishment with permission. </p>
                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-3">
                        <div class="h-2-p-c-details h-3-p-c-default">
                            <div class="h-2-p-c-default">
                                <img src="images/home-3-blog-post-4.png" class="img-responsive" alt="">
                                <h2><a href="#">Drug Offences</a></h2>
                                <p>The legal restrictions placed on the use of controlled drugs are aimed at preventing drug misuse. The principal offences are contained in the Misuse of Drugs Act 1971.</p>
                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
    </section> <!-- end practice area -->

    <!-- =========================
     START TESTMONIAL SECTION
    ============================== -->

    <section class="our-history-area type-1-bg">
        <div class="welcome-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="Title-area">
                            <h3>Our History</h3>
                            <h2>All About Trust</h2>
                            <p>Trust is a business theme perfectly suited legal advisers and offices, lawyers, attorneys, counsels, advocates and other legal and law related services.</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="asset-button btn-text-left text-right top-margin">
                            <button type="button" class="btn btn-default">Find Out More About Trust</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end title. it will use all pages title --> 

        <div class="history-content-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 no-padding">
                        <div class="history-right-content">
                            <div class="col-sm-6 col-md-6"> 
                                <div class="progress-left-img">
                                    <img src="images/progress-left-img.png" class="img-responsive" alt="">
                                    <span><i class="fa fa-shield"></i></span>                                
                                </div>  
                            </div> 
                            <div class="col-sm-6 col-md-6">
                                <div class="history-right-content-text">
                                    <p>Trust is a business theme perfectly suited legal advisers and offices, lawyers, attorneys, and other legal and law related services. We have started as a sole practitioner providing services to the area community. Aiming to provide high quality legal consultancy, support and results for your legal issues. Using their expertise and experience, Trust law firm documents and builds their clients' cases to obtain the best results they could achieve.</p>
                                    <img src="images/history-right-sign.png" alt="">
                                </div>
                            </div>
                        </div>                        
                    </div>
                    <div class="col-md-4">
                        <div class="progressbar">
                            <div class="progress_cont">
                                <div class="skill">Criminal Law<span class="pull-right"></span> </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">94% Complete (success)</span> </div>
                                </div>
                            </div>
                            <div class="progress_cont">
                                <div class="skill">Indurance <span class="pull-right"></span> </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">94% Complete (success)</span> </div>
                                </div>
                            </div>
                            <div class="progress_cont">
                                <div class="skill">Financial Law <span class="pull-right"></span> </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">94% Complete (success)</span> </div>
                                </div>
                            </div>
                            <div class="progress_cont">
                                <div class="skill">Civil Litigation <span class="pull-right"></span> </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">94% Complete (success)</span> </div>
                                </div>
                            </div>
                            <div class="progress_cont">
                                <div class="skill">Other Areas <span class="pull-right"></span> </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 0%"> <span class="sr-only">94% Complete (success)</span> </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </div>
        </div>              
    </section><!-- end testimonial area -->

    <!-- =========================
     START TESTMONIAL SECTION
    ============================== -->

    <section class="testimonial-area home-3-testimonial">
        <div class="testimonial-content-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="Title-area">
                            <h3>History</h3>
                            <h2>Why Trust !</h2>
                        </div>                        
                        <div class="history-accordion">
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default history-accordion-default">
                                    <div class="panel-heading history-panel-heading" role="tab" id="headingOne">
                                      <h4 class="panel-title history-panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><i class="fa fa-bookmark"></i>  Story & History</a>
                                      </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body history-panel-body">
                                            <img src="images/accordio-body-img.png" alt="">
                                            <div class="accordion-body-text">
                                                <p>Trust started as a sole practitioner providing services to the area community, our Office has now grown to five lawyers and provides expert legal representation. Trust Law Offices founded on the principles of truth, justice, accountability, and equal access. Our success has been the result of each attorney and staff members.</p>
                                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default history-accordion-default">
                                    <div class="panel-heading history-panel-heading" role="tab" id="headingTwo">
                                      <h4 class="panel-title history-panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><i class="fa fa-book"></i> Values & Philosophy
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body history-panel-body">
                                            <img src="images/accordio-body-img.png" alt="">
                                            <div class="accordion-body-text">
                                                <p>Trust started as a sole practitioner providing services to the area community, our Office has now grown to five lawyers and provides expert legal representation. Trust Law Offices founded on the principles of truth, justice, accountability, and equal access. Our success has been the result of each attorney and staff members.</p>
                                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default history-accordion-default">
                                    <div class="panel-heading history-panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title history-panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><i class="fa fa-briefcase"></i> Mission & Services
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body history-panel-body">
                                            <img src="images/accordio-body-img.png" alt="">
                                            <div class="accordion-body-text">
                                                <p>Trust started as a sole practitioner providing services to the area community, our Office has now grown to five lawyers and provides expert legal representation. Trust Law Offices founded on the principles of truth, justice, accountability, and equal access. Our success has been the result of each attorney and staff members.</p>
                                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default history-accordion-default">
                                    <div class="panel-heading history-panel-heading" role="tab" id="headingFour">
                                      <h4 class="panel-title history-panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour"><i class="vc_tta-icon fa fa-graduation-cap"></i> Our Skills
                                        </a>
                                      </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                        <div class="panel-body history-panel-body">
                                            <img src="images/accordio-body-img.png" alt="">
                                            <div class="accordion-body-text">
                                                <p>Trust started as a sole practitioner providing services to the area community, our Office has now grown to five lawyers and provides expert legal representation. Trust Law Offices founded on the principles of truth, justice, accountability, and equal access. Our success has been the result of each attorney and staff members.</p>
                                                <a href="#">More <i class="fa fa-long-arrow-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="Title-area">
                            <h3>All About Us</h3>
                            <h2>Our Latest News</h2>
                        </div>                        
                        <div class="t-b-content-area">
                            <div class="col-md-6 col-sm-6 no-padding-left">
                                <div class="t-b-details">
                                    <div class="t-b-img">
                                        <img src="images/testimonial-blog-post-1.png" class="img-responsive" alt="blog-post">
                                        <div><span>JUNE</span><span>30</span></div>
                                    </div>
                                    <div class="t-b-comment">
                                        <p>Posted In: <span>Legal Advice</span></p>
                                        <p><span><a href="#"><i class="fa fa-comments"></i></a> 15</span>
                                           <span><a href="#"><i class="fa fa-eye"></i></a> 11</span></p>
                                    </div>
                                    <h2><a href="#">It’s My Pleasure to graduate with excellent!</a></h2>
                                    <p>We cannot expect people to have respect for laws and orders until we teach respect to those we have entrusted to enforce those laws all the time.</p>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 no-padding-right">
                                <div class="t-b-details">
                                    <div class="t-b-img">
                                        <img src="images/testimonial-blog-post-2.png" class="img-responsive" alt="blog-post">
                                        <div><span>JUNE</span><span>29</span></div>
                                    </div>
                                    <div class="t-b-comment">
                                        <p>Posted In: <span>Law, Articles</span></p>
                                        <p><span><a href="#"><i class="fa fa-comments"></i></a> 15</span>
                                           <span><a href="#"><i class="fa fa-eye"></i></a> 11</span></p>
                                    </div>
                                    <h2><a href="#">All you want to know about industrial laws</a></h2>
                                    <p>It’s no secret that the digital industry is booming. From exciting startups to global brands, companies are reaching out to digital agencies, responding to...</p>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>
        </div>       
    </section><!-- end testimonial area -->

    <!-- =========================
     START CONTACT US SECTION
    ============================== -->

    <section class="contact-us home-3-contact-us">
        <div class="welcome-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="Title-area contact-title">
                            <h3>People Say</h3>
                            <h2>Clients Testimonials </h2>
                            <p>See what our clients say about us, we have been known for doing what we says, aiming to bring favourable results for its customers as soon as possible.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end title. it will use all pages title -->
        <div class="contact-form">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="home-3-testimonial-demo" class="owl-carousel owl-theme">
                            <div class="item">
                                <div class="owl-testimonial-content home-3-testimonial-slider">
                                    <i class="fa fa-quote-left"></i>
                                    <p>rust law firm documents and builds their clients' cases to obtain the best results they could achieve in their particular situation.</p>     
                                </div>
                                <div class="testimonial-quote home-3-testimonial-quote">
                                    <span>Begha</span>
                                    <span>Art Director, 7oroof Agency.</span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="owl-testimonial-content home-3-testimonial-slider">
                                    <i class="fa fa-quote-left"></i>
                                    <p>Aiming to provide high quality legal consultancy, support and results for your legal issues. Using their expertise and experience.</p>
                                </div>
                                <div class="testimonial-quote home-3-testimonial-quote">
                                    <span>Habaza</span>
                                    <span>UX Researcher.</span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="owl-testimonial-content home-3-testimonial-slider">
                                    <i class="fa fa-quote-left"></i>
                                    <p>Trust has been known for doing what he says, aiming to bring favourable results for its customers as soon as possible.</p>                                    
                                </div>
                                <div class="testimonial-quote home-3-testimonial-quote">
                                    <span>Ahmed Hassan</span>
                                    <span>UI Developer.</span>
                                </div>
                            </div>
                            <div class="item">
                                <div class="owl-testimonial-content home-3-testimonial-slider">
                                    <i class="fa fa-quote-left"></i>
                                    <p>Trust has been known for doing what he says, aiming to bring favourable results for its customers as soon as possible.</p>                                    
                                </div>
                                <div class="testimonial-quote home-3-testimonial-quote">
                                    <span>Ahmed Hassan</span>
                                    <span>UI Developer.</span>
                                </div>
                            </div>
                        </div>                        
                    </div>                              
                </div>                
            </div>
        </div>
    </section> <!-- end contact us area -->

    <!-- =========================
     START OUR CLIENT SECTION
    ============================== -->

    <section class="our-client-area">
        <div class="welcome-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="Title-area">
                            <h3>They Trust Us</h3>
                            <h2>Our Clients</h2>
                            <p>Results recommend Trust  Law Firm as a good lawyers office, a trusted partners of his customers’ business and a honest adviser and consultant for legal situation.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end title. it will use all pages title -->   
        <div class="client-slider">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="client-content">
                            <div id="client-demo" class="owl-carousel owl-theme">
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-1.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-2.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-3.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-4.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-5.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-6.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-1.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-2.png" alt=""></a>
                                </div>
                            </div>
                            <div class="testimonial-customNavigation client-customNavigation">
                              <a class="btn client_prev"><i class="fa fa-long-arrow-left"></i></a>
                              <a class="btn client_next"><i class="fa fa-long-arrow-right"></i></a>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </section> 
    <!-- end client area -->


<?php get_footer(); ?>