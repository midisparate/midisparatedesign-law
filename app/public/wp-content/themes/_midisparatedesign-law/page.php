<?php  get_header(); 

while(have_posts()) {
    the_post(); ?>

 <!-- =========================
     PAGE TITLE SECTION
    ============================== -->  
    <section class="page-title-area about-us-page-title-bg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-padding">
                        <div class="page-title-content text-center">
                           <h2><?php the_title(); ?></h2>
                           <p>DON'T FORGET TO REPLACE ME LATER</p> 
                        </div>
                        <?php
                        $theParent = wp_get_post_parent_id(get_the_ID());
                        if ($theParent) { ?>
                        <div class="breadcrumbs text-center">
                           <ul class="page-breadcrumbs">
                               <li><a href="<?php echo get_permalink($theParent); ?>">Back to <?php echo get_the_title($theParent);?></a></li>
                               <li><a><?php the_title(); ?></a></li>
                           </ul>                        
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section> 

    <!-- =========================
     START WELCOME SECTION
    ============================== -->

    <section class="welcome-area">
        <div class="welcome-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-8">
                        <div class="Title-area">
                            <h2><?php the_title(); ?></h2>
                            <p><?php the_content(); ?></p>
                        </div>
                    </div>
                    <?php 
                    $testArray = get_pages(array(
                    'child_of' => get_the_ID()
                    ));
                    if ($theParent or $testArray) { ?>
                    <div class="col-xs-12 col-sm-4">
                    <div class="Title-area">
                    <h2 class="page-links__title"><a href="<?php echo get_permalink($theParent); ?>"><?php echo get_the_title($theParent); ?></a></h2>
                        </div>
                        <div class="asset-content">
                        <h2><?php
                            if ($theParent) {
                                $findChildrenOf = $theParent;
                            } else {
                                $findChildrenOf = get_the_ID();
                            }

                        wp_list_pages(array(
                            'title_li' => NULL,
                            'child_of' => $findChildrenOf
                        ));
                        ?></h2>
                        </div>
                    </div>
                    <?php } ?> 
                </div>
            </div>
        </div> <!-- end title. it will use all pages title -->

    </section> <!-- end welcome area -->

    <!-- =========================
     START HOME 7 TRUST SECTION
    ============================== -->
    <section class="home-9-trust">
        <!-- START HOME 9 TRUST-->
        <div class="home-3-contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="home-9-slider">
                            <div id="home-9-demo" class="owl-carousel owl-theme">
                                <div class="item home-9-slider-content text-center">
                                    <h2>“We cannot expect people to have respect for law and order until we teach respect to those we have entrusted to enforce those laws.”</h2>
                                    <p>― Hunter S. Thompson</p>
                                </div>
                                <div class="item home-9-slider-content text-center">
                                    <h2>“We cannot expect people to have respect for law and order until we teach respect to those we have entrusted to enforce those laws.”</h2>
                                    <p>― Hunter S. Thompson</p>
                                </div>
                            </div>
                        </div> 
                    </div> 
               </div>
            </div>
        </div> <!-- end cotact --> 
    </section>

    <!-- =========================
     START OUR TEAM SECTION
    ============================== -->
    
    <section class="our-team-area about-us1-team">
        <div class="welcome-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="Title-area">
                            <h3>Our Legal Team</h3>
                            <h2>Meet Our Attorneys</h2>
                            <p>Each lawyer at Trust law firm focuses exclusively on civil matters, we have choosed our lawyers taking into account their experience and ability to handle all cases.</p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="asset-button btn-text-left text-right top-margin">
                            <button type="button" class="btn btn-default">Meet The Whole Team</button>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end title. it will use all pages title -->

        <!-- start team member introduction -->  

        <div class="team-member-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="team-describe">
                            <div class="team-content">
                                <div class="team-img">
                                    <img src="images/team-member-1.png" alt="team-member">
                                </div>
                                <div class="about-team-member text-center">
                                    <div class="team-describe-content">
                                        <p>Using his expertise and experience, Mahmoud documents and builds his customers cases to obtain the best results they could achieve in their particular situation.</p>
                                    </div>
                                    <div class="social-content-box">
                                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                                        <a href="#"> <i class="fa fa-google-plus"></i> </a>
                                        <a href="#"> <i class="fa fa  fa-linkedin"></i> </a>
                                    </div>
                                </div>
                            </div>
                            <div class="member-name text-center">
                                <h2><a href="#">Mahmoud Adel Baghagho</a></h2>
                                <p>CEO & Manager</p>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="team-describe">
                            <div class="team-content">
                                <div class="team-img">
                                    <img src="images/team-member-2.png" alt="team-member">
                                </div>
                                <div class="about-team-member text-center">
                                    <div class="team-describe-content">
                                        <p>Using his expertise and experience, Mahmoud documents and builds his customers cases to obtain the best results they could achieve in their particular situation.</p>
                                    </div>
                                    <div class="social-content-box">
                                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                                        <a href="#"> <i class="fa fa-google-plus"></i> </a>
                                        <a href="#"> <i class="fa fa  fa-linkedin"></i> </a>
                                    </div>
                                </div>
                            </div>
                            <div class="member-name text-center">
                                <h2><a href="#">Ahmed Hassan</a></h2>
                                <p>Civil Lawyer</p>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="team-describe">
                            <div class="team-content">
                                <div class="team-img">
                                    <img src="images/team-member-3.png" alt="team-member">
                                </div>
                                <div class="about-team-member text-center">
                                    <div class="team-describe-content">
                                        <p>Using his expertise and experience, Mahmoud documents and builds his customers cases to obtain the best results they could achieve in their particular situation.</p>
                                    </div>
                                    <div class="social-content-box">
                                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                                        <a href="#"> <i class="fa fa-google-plus"></i> </a>
                                        <a href="#"> <i class="fa fa  fa-linkedin"></i> </a>
                                    </div>
                                </div>
                            </div>
                            <div class="member-name text-center">
                                <h2><a href="#">Mohamed Habaza</a></h2>
                                <p>Business Lawyer</p>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="team-describe">
                            <div class="team-content">
                                <div class="team-img">
                                    <img src="images/team-member-4.png" alt="team-member">
                                </div>
                                <div class="about-team-member text-center">
                                    <div class="team-describe-content">
                                        <p>Using his expertise and experience, Mahmoud documents and builds his customers cases to obtain the best results they could achieve in their particular situation.</p>
                                    </div>
                                    <div class="social-content-box">
                                        <a href="#"> <i class="fa fa-facebook"></i> </a>
                                        <a href="#"> <i class="fa fa-twitter"></i> </a>
                                        <a href="#"> <i class="fa fa-google-plus"></i> </a>
                                        <a href="#"> <i class="fa fa  fa-linkedin"></i> </a>
                                    </div>
                                </div>
                            </div>
                            <div class="member-name text-center">
                                <h2><a href="#">Ahmed Abd Alhaleem</a></h2>
                                <p>Criminal Defence</p>
                                <span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>      
    </section>  <!-- end our team area -->

    <div class="help-area">
        <div class="left-half"></div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-6">
                    <div class="help-img">
                        <img src="images/horse.png" alt="">
                    </div>
                </div> <!-- end left side image area -->

                <div class=" col-md-6 help-custom-padding">
                    <div class="help-text-heading">
                        <h2>WE ARE HERE TO PROVIDE LEGAL HELP</h2>
                        <p>In Trust, we are aiming to provide high quality legal consultancy, support and results for your legal issues.</p>
                    </div><!--  end heading text -->

                    <!-- start content -->
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="help-content">
                                <i class="fa fa-gavel"></i>
                                <h2>Get Your Legal Advice</h2>
                                <p>Post your question and get free legal advice directly from our experienced lawyers, We are here to help you.</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="help-content">
                                <i class="fa fa-users"></i>
                                <h2>Work With Expert Lawyers</h2>
                                <p>Our lawyers are expertise and experts in all law fields, They will obtain the best results they could achieve.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="help-content">
                                <i class="fa fa-money"></i>
                                <h2>Have Great Discounted Rates</h2>
                                <p>If Trust accept your case, we'll never require you to pay huge fees or expenses as we work with alow cost.</p>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="help-content">
                                <i class="fa fa-university"></i>
                                <h2>Review Your Case Documents</h2>
                                <p>Get a thorough review of your legal documents by an expert attorney for as little as $0 per document.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end help area -->    

    <!-- =========================
     START OUR CLIENT SECTION
    ============================== -->

    <section class="our-client-area">
        <div class="welcome-title-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="Title-area">
                            <h3>They Trust Us</h3>
                            <h2>Our Clients</h2>
                            <p>Results recommend Trust  Law Firm as a good lawyers office, a trusted partners of his customers’ business and a honest adviser and consultant for legal situation.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end title. it will use all pages title -->   
        <div class="client-slider">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="client-content">
                            <div id="client-demo" class="owl-carousel owl-theme home-client-content">
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-1.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-2.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-3.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-4.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-5.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-6.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-1.png" alt=""></a>
                                </div>
                                <div class="item client-item">
                                    <a href="#"><img src="images/client-2.png" alt=""></a>
                                </div>
                            </div>
                            <div class="testimonial-customNavigation client-customNavigation">
                              <a class="btn client_prev"><i class="fa fa-long-arrow-left"></i></a>
                              <a class="btn client_next"><i class="fa fa-long-arrow-right"></i></a>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </section> <!-- end client area -->   

<?php }

get_footer(); 

?>